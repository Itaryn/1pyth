# Create a new board
# row = number of row
# col = number of column
# pawn = number of pawn rows
def newBoard(row, col, pawns):
    board = [[0] * col] * row
    for i in range(pawns):
        board[i] = [1] * col
        board[-i - 1] = [2] * col
    return board


# Display the board and the position
# 0 = .
# 1 = x
# 2 = o
def display(board):
    for i, row in enumerate(board):
        print(i + 1, end=" " * (1 + len(str(len(board))) - len(str(i))))
        for y, x in enumerate(row):
            if x == 0:
                print("." + " " * len(str(y + 1)), end="")
            elif x == 1:
                print("x" + " " * len(str(y + 1)), end="")
            elif x == 2:
                print("o" + " " * len(str(y + 1)), end="")
        print()
    print(end=" " * (1 + len(str(len(board)))))
    for i in range(len(board[0])):
        print(i + 1, end=" ")
    print("\n")


# Check if the position (i, j) is a player pawn
# We consider : if i == 1 it's the first index of the board (0 in python)
def possiblePawn(board, row, col, player, i, j):
    return inBoard(row, col, i, j) and board[i - 1][j - 1] == player and pawnCanMove(board, row, col, player, i, j)


# Check if the pawn can move to at least one direction
def pawnCanMove(board, row, col, player, i, j):
    playerDecal = 1 if player == 1 else -1
    return positionIsEmptyOrOpponent(board, row, col, player, i + 1 * playerDecal, j - 1) or \
           positionIsEmptyOrOpponent(board, row, col, player, i + 1 * playerDecal, j) or \
           positionIsEmptyOrOpponent(board, row, col, player, i + 1 * playerDecal, j + 1)


# Check  if the pawn can move to a position
def positionIsEmptyOrOpponent(board, row, col, player, i, j):
    opponent = 1 if player == 2 else 2
    return inBoard(row, col, i, j) and board[i - 1][j - 1] in [0, opponent]


# Check if the pawn (i, j) can move to the column (l)
def possibleDestination(board, row, col, player, i, j, l):
    playerDecal = 1 if player == 1 else -1
    return abs(j - l) <= 1 and positionIsEmptyOrOpponent(board, row, col, player, i + 1 * playerDecal, l)


# Check if i is in the board limitation
def inBoard(row, col, i, j=1):
    return 1 <= i <= row and 1 <= j <= col


# Ask the user to choose a position until he gives a possible one
def selectPawn(board, row, col, player):
    while True:
        i = eval(input("Choose a pawn (row) : "))
        j = eval(input("Choose a pawn (col) : "))
        if possiblePawn(board, row, col, player, i, j):
            return i, j


# Ask the user to choose a position until he gives a possible one
def selectDestination(board, row, col, player, i, j):
    while True:
        l = eval(input("Choose a destination for your pawn (" + str(i) + ", " + str(j) + ") : "))
        if possibleDestination(board, row, col, player, i, j, l):
            return l


# Change the board
def put(board, i, j, l, player):
    # Remove the player pawn
    board[i - 1][j - 1] = 0
    # Recreate the player pawn to the new position
    playerDecal = 1 if player == 1 else -1
    board[i - 1 + 1 * playerDecal][l - 1] = player


# Check if the game if finished
def again(board):
    return 2 not in board[0] and 1 not in board[-1]


# Main
def breakthrough(row, col, pawns):
    board = newBoard(row, col, pawns)
    player = 1
    while again(board):
        display(board)
        print("Player", player, "(x)" if player == 1 else "(o)")
        i, j = selectPawn(board, row, col, player)
        l = selectDestination(board, row, col, player, i, j)
        put(board, i, j, l, player)
        # Change the player
        player = 1 if player == 2 else 2
    display(board)
    # Change the player when the game is finished to see the winner
    player = 1 if player == 2 else 2
    print("Winner", player)


breakthrough(5, 4, 2)
