# Create a new board
# n = length of the board
def newBoard(n):
    return [0]*n


# Display the board and the position
# 0 = .
# 1 = x
# -1 = o
def display(board, n):
    for i, x in enumerate(board):
        if x == 0:
            printWithSpace(".", i)
        elif x == 1:
            printWithSpace("x", i)
        elif x == -1:
            printWithSpace("o", i)
    print()
    for i in range(1, n + 1):
        print(i, end=" ")
    print("\n")


# Display the symbole (., x or o) with number of space depending on the length of the position
def printWithSpace(sym, i):
    print(sym, " " * len(str(i + 1)), sep="", end="")


# Check if the position (i) is valid or not
# We consider : if i == 1 it's the first index of the board (0 in python)
def possible(board, n, i):
    return 1 <= i <= n and board[i - 1] == 0


# Ask the user to choose a position until he gives a possible one
def select(board, n):
    while True:
        number = eval(input("Choose a licit number of square : "))
        if possible(board, n, number):
            return number


# Change the board (1 for the chosen pawn, and -1 for right and left position)
def put(board, n, i):
    board[i - 1] = 1
    if possible(board, n, i - 1):
        board[i - 2] = -1
    if possible(board, n, i + 1):
        board[i] = -1


# Check if the game if finished
def again(board):
    return 0 in board


# Main
def dawson(n):
    board = newBoard(n)
    player = 1
    while again(board):
        display(board, n)
        print("Player", player)
        put(board, n, select(board, n))
        # Change the player
        player = 1 if player == 2 else 2
    display(board, n)
    # Change the player when the game is finished to see the winner
    player = 1 if player == 2 else 2
    print("Winner", player)


#dawson(12)