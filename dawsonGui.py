from dawson import *
from tkinter import *
from tkinter import messagebox

class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.squareSize = 50
        self.boardSize = 12
        self.player = 1
        self.create_widgets()
        self.board = newBoard(self.boardSize)
        self.display()

        print(self.convertPixel(20, 30))

    def create_widgets(self):
        self.canvas = Canvas(self.master, width=650, height=100)
        self.canvas.bind('<Button-1>', self.click)
        self.canvas.grid(row=0, column=0)

    def click(self, evt):
        if possible(self.board, self.boardSize, self.convertPixel(evt.x, evt.y)):
            put(self.board, self.boardSize, self.convertPixel(evt.x, evt.y))
            if not again(self.board):
                self.display()
                if messagebox.askquestion("WIN", "Player " + str(self.player) + " win !\nDo you want to restart ?") == "yes":
                    self.board = newBoard(self.boardSize)
                    self.display()
                else:
                    root.destroy()
                return
            self.player = 1 if self.player == 2 else 2
            self.display()

    def convertPixel(self, x, y):
        if y < 25 or y > 25 + self.squareSize:
            return -1
        return (x - 25) // self.squareSize + 1

    def display(self):
        self.canvas.delete("all")
        for i, x in enumerate(self.board):
            self.canvas.create_rectangle(i * self.squareSize + 25, 25, i * self.squareSize + 25 + self.squareSize, 25 + self.squareSize)
            if x == 1:
                self.canvas.create_oval(i * self.squareSize + 30, 30, i * self.squareSize + 25 + self.squareSize - 5, 25 + self.squareSize - 5, fill="red", outline="white")
            elif x == -1:
                self.canvas.create_oval(i * self.squareSize + 30, 30, i * self.squareSize + 25 + self.squareSize - 5, 25 + self.squareSize - 5, fill="orange", outline="white")

        Label(self.master, text="Player " + str(self.player)).grid(row=1, column=0)


root = Tk()
root.geometry('650x200')
app = Application(master=root)
app.mainloop()