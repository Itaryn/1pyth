import random, copy

class Tree:
    state = 0

    def __init__(self, percent):
        self.state = 0 if random.random() >= percent else 1

    def am_i_burning(self):
        return self.state == 2

    def update_state(self, change):
        if not change:
            return
        if self.state == 1:
            self.state = 2
        elif self.state == 2:
            self.state = 3

    def __repr__(self):
        if self.state == 1:
            return "T"
        elif self.state == 2:
            return "F"
        elif self.state == 3:
            return "B"
        else:
            return "."


class Forest:
    row = 10
    col = 10
    grid = []

    def __init__(self, row, col, percent):
        self.row = row
        self.col = col
        self.grid = []
        for r in range(row):
            self.grid.append([])
            for c in range(col):
                self.grid[r].append(Tree(percent))

    def __repr__(self):
        for row in self.grid:
            for x in row:
                print(x, end=" ")
            print()
        return "\n"

    def neighbor_burns(self, row, col):
        return self.valid_coord(row - 1, col) and self.grid[row - 1][col].am_i_burning() or \
               self.valid_coord(row + 1, col) and self.grid[row + 1][col].am_i_burning() or \
               self.valid_coord(row, col - 1) and self.grid[row][col - 1].am_i_burning() or \
               self.valid_coord(row, col + 1) and self.grid[row][col + 1].am_i_burning() or \
               self.grid[row][col].am_i_burning()

    def valid_coord(self, row, col):
        return 0 <= row <= self.row - 1 and 0 <= col <= self.col - 1

    def updateGeneration(self):
        gridTmp = copy.deepcopy(self.grid)
        for y in range(self.row):
            for x in range(self.col):
                gridTmp[y][x].update_state(self.neighbor_burns(y, x))
        self.grid = gridTmp

    def proportion_of_trees(self):
        number_of_trees = 0
        for row in self.grid:
            for x in row:
                if x.state == 1:
                    number_of_trees += 1
        return number_of_trees / (self.row * self.col)

    def burn_a_random_tree(self):
        finish = False
        while not finish:
            row = random.randint(0, self.row - 1)
            col = random.randint(0, self.col - 1)
            if self.grid[row][col].state == 1:
                self.grid[row][col].update_state(True)
                finish = True

    def display_generation(self, target):
        print("Generation 0 : ")
        self.__repr__()
        print("Proportion of tree :", self.proportion_of_trees())
        self.burn_a_random_tree()
        print("Generation 1 : ")
        self.__repr__()
        print("Proportion of tree :", self.proportion_of_trees())
        for i in range(target):
            self.updateGeneration()
            print("Generation", i + 1, ":")
            self.__repr__()
            print("Proportion of tree :", self.proportion_of_trees())


#forest = Forest(10, 10, 0.5)
#forest.display_generation(20)