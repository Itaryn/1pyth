from forestFire import *
import tkinter as tk

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.create_widgets()
        self.generate_forest()

    def create_widgets(self):
        self.canvas = tk.Canvas(self.master)

        tk.Label(self.master, text="Longueur : ").grid(row=0, column=10)
        self.entry_len_x = tk.Entry(self.master)
        self.entry_len_x.insert(0, "10")
        self.entry_len_x.grid(row=0, column=11)

        tk.Label(self.master, text="Hauteur : ").grid(row=1, column=10)
        self.entry_len_y = tk.Entry(self.master)
        self.entry_len_y.insert(0, "10")
        self.entry_len_y.grid(row=1, column=11)

        tk.Label(self.master, text="Pourcentage : ").grid(row=2, column=10)
        self.entry_percentage = tk.Entry(self.master)
        self.entry_percentage.insert(0, "0.5")
        self.entry_percentage.grid(row=2, column=11)

        tk.Label(self.master, text="Pourcentage actuel : ").grid(row=3, column=10)
        self.label_percent = tk.Label(self.master, text=self.entry_percentage.get())
        self.label_percent.grid(row=3, column=11)

        self.generate_forest_button = tk.Button(self.master, text="Generate Forest", command=self.generate_forest)
        self.generate_forest_button.grid(row=4, column=10, columnspan=2)
        self.new_generation_button = tk.Button(self.master, text="Next Generation", command=self.new_generation)
        self.new_generation_button.grid(row=5, column=10, columnspan=2)
        self.burn_a_tree_button = tk.Button(self.master, text="Burn a Tree", command=self.burn_a_random_tree)
        self.burn_a_tree_button.grid(row=6, column=10, columnspan=2)

    def generate_forest(self):
        self.forest = Forest(int(self.entry_len_x.get()), int(self.entry_len_y.get()), float(self.entry_percentage.get()))
        self.display_forest()

    def new_generation(self):
        self.forest.updateGeneration()
        self.display_forest()

    def burn_a_random_tree(self):
        self.forest.burn_a_random_tree()
        self.display_forest()

    def display_forest(self):
        self.canvas.delete("all")
        for i, row in enumerate(self.forest.grid):
            for y, x in enumerate(row):
                self.canvas.create_rectangle(i * 10 + 2, y * 10 + 2, i * 10 + 10 + 2, y * 10 + 10 + 2, fill=self.GetTreeColor(x))
        self.canvas.grid(row=0, column=0, rowspan=7, columnspan=10)

    def GetTreeColor(self, tree):
        if tree.state == 1:
            return "#00FF00"
        elif tree.state == 2:
            return "#FF0000"
        elif tree.state == 3:
            return "#8E8E8E"
        else:
            return "#FFFFFF"

root = tk.Tk()
app = Application(master=root)
app.mainloop()