import random, copy, time


class Cell:
    alive = False

    def __init__(self, percent=0.5):
        self.alive = random.random() <= percent

    def update_state(self, neighbors):
        if self.alive:
            if neighbors <= 1 or neighbors >= 4:
                self.alive = False
        else:
            if neighbors == 3:
                self.alive = True

    def __repr__(self):
        return "X" if self.alive else "."


class World:
    row = 10
    col = 10
    grid = []

    def __init__(self, row, col, percent=0.5):
        self.row = row
        self.col = col
        self.grid = []
        for r in range(row):
            self.grid.append([])
            for c in range(col):
                self.grid[r].append(Cell(percent))

    def __repr__(self):
        for row in self.grid:
            for x in row:
                print(x, end=" ")
            print()

        return "\n"

    def neighbors_count(self, row, col):
        neighbors = 0
        for y in range(-1, 2):
            for x in range(-1, 2):
                if y == 0 and x == 0:
                    continue
                if self.valid_coord(row + y, col + x) and self.grid[row + y][col + x].alive:
                    neighbors += 1
        return neighbors

    def valid_coord(self, row, col):
        return 0 <= row <= self.row - 1 and 0 <= col <= self.col - 1

    def updateGeneration(self):
        gridTmp = copy.deepcopy(self.grid)
        for y in range(self.row):
            for x in range(self.col):
                gridTmp[y][x].update_state(self.neighbors_count(y, x))
        self.grid = gridTmp

    def proportion_of_alive_cell(self):
        number_of_cell = 0
        for row in self.grid:
            for x in row:
                if x.alive:
                    number_of_cell += 1
        return number_of_cell / (self.row * self.col)

    def display_generation(self, target):
        print("Generation 0 : ")
        self.__repr__()
        print("Proportion of alive cell :", self.proportion_of_alive_cell())
        for i in range(target):
            print("\n"*20)
            self.updateGeneration()
            print("Generation", i + 1, ":")
            self.__repr__()
            print("Proportion of alive cell :", self.proportion_of_alive_cell())
            time.sleep(1)


#world = World(15, 15)
#world.display_generation(100)