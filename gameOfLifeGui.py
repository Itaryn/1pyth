from gameOfLife import *
from tkinter import *

class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.create_widgets()
        self.generate_world()

    def create_widgets(self):
        self.canvas = Canvas(self.master)

        Label(self.master, text="Longueur : ").grid(row=0, column=10)
        self.entry_len_x = Entry(self.master)
        self.entry_len_x.insert(0, "10")
        self.entry_len_x.grid(row=0, column=11)

        Label(self.master, text="Hauteur : ").grid(row=1, column=10)
        self.entry_len_y = Entry(self.master)
        self.entry_len_y.insert(0, "10")
        self.entry_len_y.grid(row=1, column=11)

        Label(self.master, text="Pourcentage : ").grid(row=2, column=10)
        self.entry_percentage = Entry(self.master)
        self.entry_percentage.insert(0, "0.5")
        self.entry_percentage.grid(row=2, column=11)

        Label(self.master, text="Pourcentage actuel : ").grid(row=3, column=10)
        self.label_percent = Label(self.master, text=self.entry_percentage.get())
        self.label_percent.grid(row=3, column=11)

        self.generate_forest_button = Button(self.master, text="Generate World", command=self.generate_world)
        self.generate_forest_button.grid(row=4, column=10, columnspan=2)
        self.new_generation_button = Button(self.master, text="Next Generation", command=self.new_generation)
        self.new_generation_button.grid(row=5, column=10, columnspan=2)
        self.launch_life_toggleButton = Button(self.master, text="On / Off Auto Generation", command=self.toggle_button)
        self.launch_life_toggleButton.grid(row=6, column=10, columnspan=2)

        self.auto_gen_state = BooleanVar()
        self.launch_life_checkbutton = Checkbutton(self.master, text="Auto Generation State", state="disabled", variable=self.auto_gen_state)
        self.launch_life_checkbutton.grid(row=7, column=10, columnspan=2)

    def generate_world(self):
        self.world = World(int(self.entry_len_x.get()), int(self.entry_len_y.get()), float(self.entry_percentage.get()))
        self.display_world()

    def new_generation(self):
        self.world.updateGeneration()
        self.display_world()

    def display_world(self):
        self.canvas.delete("all")
        for i, row in enumerate(self.world.grid):
            for y, x in enumerate(row):
                self.canvas.create_rectangle(i * 10 + 2, y * 10 + 2, i * 10 + 10 + 2, y * 10 + 10 + 2, fill=self.GetCellColor(x))
        self.canvas.grid(row=0, column=0, rowspan=7, columnspan=10)

    def toggle_button(self):
        self.launch_life_checkbutton.toggle()
        self.run_life()

    def run_life(self):
        if self.auto_gen_state.get() == True:
            self.new_generation()
            self.master.after(150, self.run_life)

    def GetCellColor(self, cell):
        return "#FF0000" if cell.alive else "#FFFFFF"

root = Tk()
app = Application(master=root)
app.mainloop()