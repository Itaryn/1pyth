import random, copy, time


class Pawn:
    coord = [0, 0]
    lastCoord = [0, 0]

    def get_coord(self):
        return self.coord

    def set_coord(self, x, y):
        self.lastCoord = self.coord
        self.coord = [x, y]

    def __init__(self, x, y):
        self.coord = [x, y]

    def move(self, board, pawn):
        finish = False
        while not finish:
            row = int(input("Choose the destination row : ")) - 1
            col = int(input("Choose the destination col : ")) - 1
            if board.valid_coord(row, col) and self.can_move(board, pawn, row, col):
                return row, col

    def can_move_directly(self, board, row, col):
        return False

    def can_move(self, board, pawn, row, col):
        return board.valid_coord(row, col) and \
               self.can_move_directly(board.get_grid(), row, col) and \
               not pawn.can_move_directly(board.get_grid(), row, col) and \
               self.lastCoord != [col, row]


class WhitePawn(Pawn):
    def can_move_directly(self, board, row, col):
        return col == self.coord[0] or row == self.coord[1] or self.cam_move_diag(board, row, col)

    def cam_move_diag(self, board, row, col):
        # Diag bottom-right
        for y, x in zip(range(self.coord[1] + 1, len(board)), range(self.coord[0] + 1, len(board[0]))):
            if y == row and x == col:
                return True
        # Diag upper-right
        for y, x in zip(range(self.coord[1] - 1, -1, -1), range(self.coord[0] + 1, len(board[0]))):
            if y == row and x == col:
                return True
        # Diag bottom-left
        for y, x in zip(range(self.coord[1] + 1, len(board)), range(self.coord[0] - 1, -1, -1)):
            if y == row and x == col:
                return True
        # Diag upper-left
        for y, x in zip(range(self.coord[1] - 1, -1, -1), range(self.coord[0] - 1, -1, -1)):
            if y == row and x == col:
                return True
        return False

    def __repr__(self):
        return "W"


class BlackPawn(Pawn):
    def can_move_directly(self, board, row, col):
        return self.coord[1] - 1 <= row <= self.coord[1] + 1 and \
               self.coord[0] - 1 <= col <= self.coord[0] + 1

    def has_lost(self, board):
        return self.coord == [len(board[0]) - 1, len(board) - 1]

    def draw(self, board, pawn):
        for y in range(-1, 2):
            for x in range(-1, 2):
                if self.can_move(board, pawn, self.coord[1] + x, self.coord[0] + y):
                    return False
        return True

    def __repr__(self):
        return "B"



class GameBoard:
    row = 10
    col = 10
    grid = []
    blackPawn = BlackPawn(0, 0)
    whitePawn = WhitePawn(0, 0)
    round = 0

    def __init__(self, row=10, col=10):
        self.row = row
        self.col = col

        for r in range(row):
            self.grid.append([])
            for c in range(col):
                self.grid[r].append(".")

        blackRow = random.randint(0, row - 1)
        blackCol = random.randint(0, col - 1)

        whiteRow = random.randint(0, row - 1)
        whiteCol = random.randint(0, col - 1)

        self.blackPawn = BlackPawn(blackCol, blackRow)
        self.whitePawn = WhitePawn(whiteCol, whiteRow)

        self.grid[blackRow][blackCol] = self.blackPawn
        self.grid[whiteRow][whiteCol] = self.whitePawn

    def __repr__(self):
        for i, row in enumerate(self.grid):
            print(i + 1, end=" " * (1 + len(str(len(self.grid))) - len(str(i))))
            for y, x in enumerate(row):
                print(x, end=" " * len(str(y + 1)))
            print()
        print(end=" " * (1 + len(str(len(self.grid)))))
        for i in range(len(self.grid[0])):
            print(i + 1, end=" ")
        return "\n"

    def get_grid(self):
        return self.grid

    def get_white_pawn(self):
        return self.whitePawn

    def get_black_pawn(self):
        return self.blackPawn

    def valid_coord(self, row, col):
        return 0 <= row <= self.row - 1 and 0 <= col <= self.col - 1

    def move_pawn(self, pawn, row, col):
        self.grid[pawn.get_coord()[1]][pawn.get_coord()[0]] = "."
        self.grid[row][col] = pawn
        pawn.set_coord(col, row)

    def again(self):
        return not self.blackPawn.draw(self, self.whitePawn) and \
               not self.blackPawn.has_lost(self.grid) and \
               self.round < 20


def mamichess(row, col):
    board = GameBoard(row, col)
    player = 1
    while board.again():
        print(board)
        print("Player", player, "(x)" if player == 1 else "(o)")

        if player == 1:
            row, col = board.get_white_pawn().move(board, board.get_black_pawn())
            board.move_pawn(board.get_white_pawn(), row, col)
        else:
            row, col = board.get_black_pawn().move(board, board.get_white_pawn())
            board.move_pawn(board.get_black_pawn(), row, col)

        # Change the player
        player = 1 if player == 2 else 2
    print(board)

    if board.get_black_pawn().draw(board, board.get_white_pawn()):
        print("Draw")
    elif board.get_black_pawn().has_lost(board.get_grid()):
        print("White win")
    else:
        print("Black win")

mamichess(5, 5)