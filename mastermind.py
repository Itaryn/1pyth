class Mastermind:
    def __init__(self, numberOfTry):
        self.board = self.newBoard(numberOfTry)

    def newBoard(self, numberOfTry):
        board = []
        for i in range(numberOfTry):
            type1 = []
            type2 = []
            for y in range(4):
                type1.append(-1)
            for y in range(4):
                type2.append(0)
            board.append([type1, type2])
        return board