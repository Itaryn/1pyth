from mastermind import *
from tkinter import *
from tkinter import messagebox

class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.squareSize = squareSize
        self.tryNumber = tryNumber
        self.mastermind = Mastermind(self.tryNumber)
        self.create_widgets()
        self.display()

        print(self.convertPixel(20, 30))

    def create_widgets(self):
        self.canvas = Canvas(self.master, width=600, height=taille, bg="grey")
        self.canvas.bind('<Button-1>', self.click)
        self.canvas.grid(row=0, column=0)


    def click(self, evt):
        if possible(self.board, self.boardSize, self.convertPixel(evt.x, evt.y)):
            put(self.board, self.boardSize, self.convertPixel(evt.x, evt.y))
            if not again(self.board):
                self.display()
                if messagebox.askquestion("WIN", "Player " + str(self.player) + " win !\nDo you want to restart ?") == "yes":
                    self.board = newBoard(self.boardSize)
                    self.display()
                else:
                    root.destroy()
                return
            self.player = 1 if self.player == 2 else 2
            self.display()

    def convertPixel(self, x, y):
        if y < 25 or y > 25 + self.squareSize:
            return -1
        return (x - 25) // self.squareSize + 1

    def display(self):
        self.canvas.delete("all")
        for i, line in enumerate(self.mastermind.board):
            for y in range(6):
                self.canvas.create_rectangle(y * self.squareSize + 50,
                                             i * self.squareSize + 100,
                                             y * self.squareSize + 50 + self.squareSize,
                                             i * self.squareSize + 100 + self.squareSize)
                if y == 0:
                    self.canvas.create_text(y * self.squareSize + self.squareSize//2 + 50,
                                            i * self.squareSize + self.squareSize//2 + 100,
                                            fill="black",
                                            text=str(i + 1))
            for y, x in enumerate(line[0]):
                if x != -1:
                    self.canvas.create_oval(y * self.squareSize + self.squareSize + 60,
                                            i * self.squareSize + 110,
                                            y * self.squareSize + 40 + self.squareSize * 2,
                                            i * self.squareSize + 90 + self.squareSize,
                                            fill=colors[x])
            for y, x in enumerate(line[1]):
                color = "black"
                if x == 1:
                    color = "red"
                elif x == 2:
                    colorOutline = "black"
                self.canvas.create_oval(self.squareSize * 5 + 55 + (0 if y < 2 else 1) * 25,
                                        i * self.squareSize + 105 + y % 2 * 50 // 2,
                                        self.squareSize * 5 + 70 + (0 if y < 2 else 1) * 25,
                                        i * self.squareSize + 120 + y % 2 * 50 // 2,
                                        fill=color)

        for i, x in enumerate(colors):
            print(i, x)
            self.canvas.create_oval(i * self.squareSize + 70,
                                    40,
                                    i * self.squareSize + 90,
                                    60,
                                    fill="red")


squareSize = 50
tryNumber = 10
colors = ["green", "blue", "orange", "red", "yellow", "purple"]

taille = 4*squareSize + tryNumber * squareSize

root = Tk()
root.geometry('600x' + str(taille))
app = Application(master=root)
app.mainloop()