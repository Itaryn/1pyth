# Create a new board
# n = length of the board
# p = number of pawn
def newBoard(n, p):
    if p >= n // 2:
        p = n // 2 - 1
    return [1] * p + [0] * (n - p * 2) + [2] * p


# Display the board and the position
# 0 = .
# 1 = x
# 2 = o
def display(board, n):
    for i, x in enumerate(board):
        if x == 0:
            printWithSpace(".", i)
        elif x == 1:
            printWithSpace("x", i)
        elif x == 2:
            printWithSpace("o", i)
    print()
    for i in range(1, n + 1):
        print(i, end=" ")
    print("\n")


# Display the symbole (., x or o) with number of space depending on the length of the position
def printWithSpace(sym, i):
    print(sym, " " * len(str(i + 1)), sep="", end="")


# Check if the position (i) is valid or not
# We consider : if i == 1 it's the first index of the board (0 in python)
def possible(board, n, pawn, player):
    decalPlayer = -1 if player == 2 else 1
    if inBoard(n, pawn):
        # Check if the pawn correspond to the actual player pawn
        if board[pawn - 1] == player:
            # Case if we move the pawn without jump over opponent pawn
            if inBoard(n, pawn + 1 * decalPlayer) and board[pawn - 1 + 1 * decalPlayer] == 0:
                return 1
            opponent = 1 if player == 2 else 2
            # Case if we jump over the opponent pawn
            if inBoard(n, pawn + 2 * decalPlayer) and board[pawn - 1 + 1 * decalPlayer] == opponent and board[pawn - 1 + 2 * decalPlayer] == 0:
                return 2
    return 0


# Check if i is in the board limitation
def inBoard(n, i):
    return 1 <= i <= n


# Ask the user to choose a position until he gives a possible one
def select(board, n, player):
    while True:
        pawn = eval(input("Choose a pawn : "))
        decal = possible(board, n, pawn, player)
        if decal != 0:
            return pawn, decal


# Change the board
def put(board, pawn, i, player):
    # Remove the player pawn
    board[pawn - 1] = 0
    # Recreate the player pawn to the new position
    decalPlayer = -1 if player == 2 else 1
    board[pawn - 1 + i * decalPlayer] = player


# Check if the game if finished
def again(board):
    return board[0] != 2 and board[-1] != 1


# Main
def toadsAndFrogs(n, p):
    board = newBoard(n, p)
    player = 1
    while again(board):
        display(board, n)
        print("Player", player, "(x)" if player == 1 else "(o)")
        pawn, i = select(board, n, player)
        put(board, pawn, i, player)
        # Change the player
        player = 1 if player == 2 else 2
    display(board, n)
    # Change the player when the game is finished to see the winner
    player = 1 if player == 2 else 2
    print("Winner", player)


#toadsAndFrogs(12, 7)